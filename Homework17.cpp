
#include <iostream>

class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}

    void VectorLength()
    {
        std::cout << sqrt(x * x + y * y + z * z);
    }
    
    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z << std::endl;
    }
};
    

int main()
{
    Vector v1;
    v1.Show();
    Vector v2(10, 11, 12);
    v2.Show();
    v2.VectorLength();
}
